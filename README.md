# TinyURL
TinyURL is a URL shortening web service, which provides short aliases for redirection of long URLs. [[Wikipedia](https://en.wikipedia.org/wiki/TinyURL)]

For instance, "**Dnmk66mU**" is the unique identifier for the page "https://www.google.com". The unique identifier can be any nonempty string with **62 alphanumeric** characters containing '0'-'9', 'a'-'z' and 'A'-'Z'. In this case, the length of identifier is fixed 8 only. Further, random characters from the 62 alphanumeric characters form the identifier. If the generated identifier collides with some previously generated identifiers, it will generate a new one.

To limit the TinyURL system size, this case use some [page replacement algorithms](https://en.wikipedia.org/wiki/Page_replacement_algorithm) including and not limited to **Least Recently Used** (LRU) and **Least Frequently Used** (LFU). The above two algorithms can be implemented with [Hash table](https://en.wikipedia.org/wiki/Hash_table) and [Doubly linked list](https://en.wikipedia.org/wiki/Doubly_linked_list). Provides 2 interfaces: encode(longUrl -> shortUrl) and decode(shortUrl -> longUrl). Calling any interface will change the position in the [Doubly linked list](https://en.wikipedia.org/wiki/Doubly_linked_list). 

## Least Recently Used (LRU) Cache Replacement Policy

```cpp
class TinyLRUCache {
public:
	TinyLRUCache(int shortUrlSize = 8, int cacheCapacity = 2);
	string encode(string longUrl);
	string decode(string shortUrl);
	void debug(void);
private:
	size_t shortUrlSize;
	size_t cacheCapacity;
	string table = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	list<pair<string, string>> l;
	unordered_map<string, list<pair<string, string>>::iterator> h1;
	unordered_map<string, list<pair<string, string>>::iterator> h2;

	string genShortUrl(void);
};
```

**decode** (shortUrl): Get the longUrl of shortUrl if the shortUrl exists in the cache, otherwise return empty string.

**encode** (longUrl): Insert if the longUrl is not already present. When the cache reaches its capacity, it should invalidate the **least recently used** item before inserting a new item. 

Sample:

1. LRU Cache (shortUrlSize = 8, cacheCapacity = 2)
```cpp
a = tinylrucache.decode("https://www.google.com");
```
```
```
```cpp
b = tinylrucache.encode("https://www.google.com");
```
```
(https://www.google.com,5pn2QMwQ)
```
```cpp
c = tinylrucache.encode("https://www.facebook.com");
```
```
(https://www.google.com,5pn2QMwQ) (https://www.facebook.com,bLq9WqJ9)
```
```cpp
d = tinylrucache.encode("https://www.google.com");
```
```
(https://www.facebook.com,bLq9WqJ9) (https://www.google.com,5pn2QMwQ)
```
```cpp
e = tinylrucache.decode(c);
```
```
(https://www.google.com,5pn2QMwQ) (https://www.facebook.com,bLq9WqJ9)
```
```cpp
f = tinylrucache.encode("https://www.linkedin.com");
```
```
(https://www.facebook.com,bLq9WqJ9) (https://www.linkedin.com,9NSqZLDv)
```
```cpp
g = tinylrucache.decode(f);
```
```
(https://www.facebook.com,bLq9WqJ9) (https://www.linkedin.com,9NSqZLDv)
```
---
2. LRU Cache (shortUrlSize = 8, cacheCapacity = 3)
```cpp
a = tinylrucache.decode("https://www.google.com");
```
```

```
```cpp
b = tinylrucache.encode("https://www.google.com");
```
```
(https://www.google.com,5pn2QMwQ)
```
```cpp
c = tinylrucache.encode("https://www.linkedin.com");
```
```
(https://www.google.com,5pn2QMwQ) (https://www.linkedin.com,bLq9WqJ9)
```
```cpp
d = tinylrucache.encode("https://www.facebook.com");
```
```
(https://www.google.com,5pn2QMwQ) (https://www.linkedin.com,bLq9WqJ9) (https://www.facebook.com,9NSqZLDv)
```
```cpp
e = tinylrucache.encode("https://www.amazon.com");
```
```
(https://www.linkedin.com,bLq9WqJ9) (https://www.facebook.com,9NSqZLDv) (https://www.amazon.com,i5zUgxBe)
```
```cpp
f = tinylrucache.decode(b);
```
```
(https://www.linkedin.com,bLq9WqJ9) (https://www.facebook.com,9NSqZLDv) (https://www.amazon.com,i5zUgxBe)
```
```cpp
g = tinylrucache.decode(d);
```
```
(https://www.linkedin.com,bLq9WqJ9) (https://www.amazon.com,i5zUgxBe) (https://www.facebook.com,9NSqZLDv)
```
```cpp
h = tinylrucache.encode("https://www.google.com");
```
```
(https://www.amazon.com,i5zUgxBe) (https://www.facebook.com,9NSqZLDv) (https://www.google.com,wrk5rHrx)
```
```cpp
i = tinylrucache.decode(c);
```
```
(https://www.amazon.com,i5zUgxBe) (https://www.facebook.com,9NSqZLDv) (https://www.google.com,wrk5rHrx)
```
```cpp
j = tinylrucache.encode("https://www.youtube.com");
```
```
(https://www.facebook.com,9NSqZLDv) (https://www.google.com,wrk5rHrx) (https://www.youtube.com,DcDg5Ng5)
```
```cpp
k = tinylrucache.decode(e);
```
```
(https://www.facebook.com,9NSqZLDv) (https://www.google.com,wrk5rHrx) (https://www.youtube.com,DcDg5Ng5)
```
```cpp
l = tinylrucache.decode(j);
```
```
(https://www.facebook.com,9NSqZLDv) (https://www.google.com,wrk5rHrx) (https://www.youtube.com,DcDg5Ng5)
```
## Least Frequently Used (LFU) Cache Replacement Policy

```cpp
class TinyUrlLFUCache {
public:
	TinyUrlLFUCache(int shortUrlSize = 8, int cacheCapacity = 2);
	string encode(string longUrl);
	string decode(string shortUrl);
	void debug(void);
private:
	size_t shortUrlSize;
	size_t cacheCapacity;
	string table = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	list<pair<size_t, list<pair<string, string>>>> l;
	unordered_map<string, pair<list<pair<size_t, list<pair<string, string>>>>::iterator, list<pair<string, string>>::iterator>> h1;
	unordered_map<string, pair<list<pair<size_t, list<pair<string, string>>>>::iterator, list<pair<string, string>>::iterator>> h2;
	string genShortUrl(void);
};
```

**decode** (shortUrl): Get the longUrl of shortUrl if the shortUrl exists in the cache, otherwise return empty string.

**encode** (longUrl): Insert if the longUrl is not already present. When the cache reaches its capacity, it should invalidate the **least frequently used** item before inserting a new item, when there is a tie(i.e., two or more items that have the same frequency), the **lease recently used** items would be evicted.

1. LFU Cache (shortUrlSize = 8, cacheCapacity = 2)
```cpp
a = tinyurllfucache.decode("https://www.google.com");
```
```

```
```cpp
b = tinyurllfucache.encode("https://www.google.com");
```
```
1: (https://www.google.com,5pn2QMwQ)
```
```cpp
c = b;
c.front()++;
d = tinyurllfucache.decode(c);
```
```
1: (https://www.google.com,5pn2QMwQ)
```
```cpp
e = tinyurllfucache.encode("https://www.facebook.com");
```
```
1: (https://www.google.com,5pn2QMwQ) (https://www.facebook.com,bLq9WqJ9)
```
```cpp
f = tinyurllfucache.encode("https://www.linkedin.com");
```
```
1: (https://www.facebook.com,bLq9WqJ9) (https://www.linkedin.com,9NSqZLDv)
```
```cpp
g = tinyurllfucache.decode(b);
```
```
1: (https://www.facebook.com,bLq9WqJ9) (https://www.linkedin.com,9NSqZLDv)
```
```cpp
h = tinyurllfucache.decode(e);
```
```
1: (https://www.linkedin.com,9NSqZLDv)
2: (https://www.facebook.com,bLq9WqJ9)
```
```cpp
i = tinyurllfucache.decode(f);
```
```
2: (https://www.facebook.com,bLq9WqJ9) (https://www.linkedin.com,9NSqZLDv)
```
```cpp
j = tinyurllfucache.encode("https://www.facebook.com");
```
```
2: (https://www.linkedin.com,9NSqZLDv)
3: (https://www.facebook.com,bLq9WqJ9)
```
```cpp
k = tinyurllfucache.encode("https://www.facebook.com");
```
```
2: (https://www.linkedin.com,9NSqZLDv)
4: (https://www.facebook.com,bLq9WqJ9)
```
```cpp
l = tinyurllfucache.encode("https://www.facebook.com");
```
```
2: (https://www.linkedin.com,9NSqZLDv)
5: (https://www.facebook.com,bLq9WqJ9)
```
```cpp
m = tinyurllfucache.encode("https://www.google.com");
```
```
1: (https://www.google.com,i5zUgxBe)
5: (https://www.facebook.com,bLq9WqJ9)
```
```cpp
n = tinyurllfucache.decode(m);
```
```
2: (https://www.google.com,i5zUgxBe) 
5: (https://www.facebook.com,bLq9WqJ9)
```
---
2. LFU Cache (shortUrlSize = 8, cacheCapacity = 3)
```cpp
a = tinyurllfucache.decode("https://www.youtube.com");
```
```

```
```cpp
b = tinyurllfucache.encode("https://www.youtube.com");
```
```
1: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
c = tinyurllfucache.decode(b);
```
```
2: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
d = tinyurllfucache.encode("https://www.youtube.com");
```
```
3: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
e = tinyurllfucache.encode("https://www.youtube.com");
```
```
4: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
f = tinyurllfucache.encode("https://www.youtube.com");
```
```
5: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
e = tinyurllfucache.decode("https://www.facebook.com");
```
```
5: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
f = tinyurllfucache.encode("https://www.facebook.com");
```
```
1: (https://www.facebook.com,bLq9WqJ9)
5: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
g = tinyurllfucache.decode(f);
```
```
2: (https://www.facebook.com,bLq9WqJ9)
5: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
h = tinyurllfucache.encode("https://www.google.com");
```
```
1: (https://www.google.com,9NSqZLDv)
2: (https://www.facebook.com,bLq9WqJ9)
5: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
i = tinyurllfucache.encode("https://www.linkedin.com");
```
```
1: (https://www.linkedin.com,i5zUgxBe)
2: (https://www.facebook.com,bLq9WqJ9)
5: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
j = tinyurllfucache.decode(h);
```
```
1: (https://www.linkedin.com,i5zUgxBe)
2: (https://www.facebook.com,bLq9WqJ9)
5: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
k = tinyurllfucache.decode(i);
```
```
2: (https://www.facebook.com,bLq9WqJ9) (https://www.linkedin.com,i5zUgxBe)
5: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
l = tinyurllfucache.encode("https://www.amazon.com");
```
```
1: (https://www.amazon.com,wrk5rHrx)
2: (https://www.linkedin.com,i5zUgxBe)
5: (https://www.youtube.com,5pn2QMwQ)
```
```cpp
m = tinyurllfucache.decode(f);
```
```
1: (https://www.amazon.com,wrk5rHrx)
2: (https://www.linkedin.com,i5zUgxBe)
5: (https://www.youtube.com,5pn2QMwQ)
```