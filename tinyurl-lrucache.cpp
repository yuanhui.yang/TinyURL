// TinyUrl LRUCache

#include <iostream> // std::cout; std::cin
#include <fstream> // std::fstream::open; std::fstream::close; 
#include <cstdlib> // rand
#include <cassert> // assert
#include <cctype> // isalnum; isalpha; isdigit; islower; isupper; isspace; tolower; toupper
#include <cmath> // pow; sqrt; round; fabs; abs; log
#include <climits> // INT_MIN; INT_MAX; LLONG_MIN; LLONG_MAX; ULLONG_MAX
#include <cfloat> // DBL_EPSILON; LDBL_EPSILON
#include <cstring> // std::memset
#include <algorithm> // std::swap; std::max; std::min; std::min_element; std::max_element; std::minmax_element; std::next_permutation; std::prev_permutation; std::nth_element; std::sort; std::lower_bound; std::upper_bound; std::reverse
#include <limits> // std::numeric_limits<int>::min; std::numeric_limits<int>::max; std::numeric_limits<double>::epsilon; std::numeric_limits<long double>::epsilon;
#include <numeric> // std::accumulate; std::iota
#include <string> // std::to_string; std::string::npos; std::stoul; std::stoull; std::stoi; std::stol; std::stoll; std::stof; std::stod; std::stold; 
#include <list> // std::list::merge; std::list::splice; std::list::merge; std::list::unique; std::list::sort
#include <bitset>
#include <vector>
#include <deque>
#include <stack> // std::stack::top; std::stack::pop; std::stack::push
#include <queue> // std::queue::front; std::queue::back; std::queue::pop; std::queue::push
#include <set> // std::set::count; std::set::find; std::set::equal_range; std::set::lower_bound; std::set::upper_bound
#include <map> // std::map::count; std::map::find; std::map::equal_range; std::map::lower_bound; std::map::upper_bound
#include <unordered_set>
#include <unordered_map>
#include <utility> // std::pair; std::make_pair
#include <iterator>
#include <functional> // std::less<int>; std::greater<int>
using namespace std;

class TinyLRUCache {
public:
	TinyLRUCache(int shortUrlSize = 8, int cacheCapacity = 2) {
		this->shortUrlSize = shortUrlSize > 0 ? shortUrlSize : 0;
		this->cacheCapacity = cacheCapacity > 0 ? cacheCapacity : 0;
		this->l.clear();
		this->h1.clear();
		this->h2.clear();
	}
	string encode(string longUrl) {
		if (shortUrlSize == 0 or cacheCapacity == 0) {
			return "";
		}
		if (!h1.empty() and h1.count(longUrl)) {
			list<pair<string, string>>::iterator p = h1.at(longUrl);
			string shortUrl = p->second;
			l.splice(end(l), l, p);
			return shortUrl;
		}
		if (h1.size() == cacheCapacity) {
			h1.erase(l.front().first);
			h2.erase(l.front().second);
			l.pop_front();
		}
		string shortUrl = genShortUrl();
		l.push_back(make_pair(longUrl, shortUrl));
		h1[longUrl] = prev(end(l));
		h2[shortUrl] = prev(end(l));
		return shortUrl;
	}
	string decode(string shortUrl) {
		if (shortUrlSize == 0 or cacheCapacity == 0 or h2.empty() or !h2.count(shortUrl)) {
			return "";
		}
		list<pair<string, string>>::iterator p = h2.at(shortUrl);
		string longUrl = p->first;
		l.splice(end(l), l, p);
		return longUrl;
	}
	void debug(void) {
		cout << "\n===\n";
		for (const auto &i : l) {
			cout << '(' << i.first << ',' << i.second << ") ";
		}
		cout << "\n===\n";
	}
private:
	size_t shortUrlSize;
	size_t cacheCapacity;
	string table = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	list<pair<string, string>> l;
	unordered_map<string, list<pair<string, string>>::iterator> h1;
	unordered_map<string, list<pair<string, string>>::iterator> h2;

	string genShortUrl(void) {
		string shortUrl(shortUrlSize, table.front());
		do {
			for (auto &i : shortUrl) {
				size_t id = rand() % table.size();
				char ch = table.at(id);
				i = ch;
			}
		} while (!h2.empty() and h2.count(shortUrl));
		return shortUrl;
	}
};

int main(void) {
	TinyLRUCache tinylrucache;

	string a, b, c, d, e, f, g, h, i, j, k, l;
	a = tinylrucache.decode("https://www.google.com");
	tinylrucache.debug();
	assert(a.empty());
	b = tinylrucache.encode("https://www.google.com");
	tinylrucache.debug();
	c = tinylrucache.encode("https://www.facebook.com");
	tinylrucache.debug();
	d = tinylrucache.encode("https://www.google.com");
	tinylrucache.debug();
	assert(b == d);
	e = tinylrucache.decode(c);
	tinylrucache.debug();
	assert("https://www.facebook.com" == e);
	f = tinylrucache.encode("https://www.linkedin.com");
	tinylrucache.debug();
	g = tinylrucache.decode(f);
	tinylrucache.debug();
	assert("https://www.linkedin.com" == g);

	tinylrucache = TinyLRUCache(8, 3);
	a = tinylrucache.decode("https://www.google.com");
	tinylrucache.debug();
	assert(a.empty());
	b = tinylrucache.encode("https://www.google.com");
	tinylrucache.debug();
	c = tinylrucache.encode("https://www.linkedin.com");
	tinylrucache.debug();
	d = tinylrucache.encode("https://www.facebook.com");
	tinylrucache.debug();
	e = tinylrucache.encode("https://www.amazon.com");
	tinylrucache.debug();
	f = tinylrucache.decode(b);
	tinylrucache.debug();
	assert(f.empty());
	g = tinylrucache.decode(d);
	tinylrucache.debug();
	assert("https://www.facebook.com" == g);
	h = tinylrucache.encode("https://www.google.com");
	tinylrucache.debug();
	i = tinylrucache.decode(c);
	tinylrucache.debug();
	assert(i.empty());
	j = tinylrucache.encode("https://www.youtube.com");
	tinylrucache.debug();
	k = tinylrucache.decode(e);
	tinylrucache.debug();
	assert(k.empty());
	l = tinylrucache.decode(j);
	tinylrucache.debug();
	assert("https://www.youtube.com" == l);

	cout << "\nPassed All\n";
	return 0;
}